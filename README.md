[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c5758efba35e47938f788dff4f8d1ecc)](https://www.codacy.com/app/torrisi-mario/helloworld?utm_source=mtorrisi@bitbucket.org&amp;utm_medium=referral&amp;utm_content=mtorrisi/helloworld&amp;utm_campaign=Badge_Grade)

## Simple Test application

This simple application was used to develop and test the module used to interact with database and [gLibrary 2.0](https://csgf.readthedocs.io/en/latest/glibrary/docs/glibrary2.html).

It consists of a really trivial user interface to test the API provided by the library under lib/ path.

## Contributors
Mario Torrisi
Vittorio Sorbera 
