exports.initDB = function () {

  var db = Ti.Database.open('SmartME');

  //Table patient
  db.execute('CREATE TABLE IF NOT EXISTS patient (idPatient TEXT PRIMARY KEY, ' +
    'name TEXT NOT NULL, surname TEXT NOT NULL, birthdate TEXT NOT NULL, ' +
    'gender TEXT NOT NULL, educationYears INTEGER, diagnosis TEXT, ' +
    'idDoctor TEXT NOT NULL);');

  //Table session
  db.execute('CREATE TABLE IF NOT EXISTS session (' +
    'idSession INTEGER PRIMARY KEY AUTOINCREMENT, idPatient TEXT NOT NULL, ' +
    'idDoctor TEXT NOT NULL, dayDate TEXT NOT NULL, startTs TEXT NOT NULL,' +
    'endTs TEXT, sent INTEGER NOT NULL, ' +
    'FOREIGN KEY(idPatient) REFERENCES patient(idPatient));');

  /*Table session_task to implement n:n relation between session and task_x
  results based on taskNumber field*/
  db.execute('CREATE TABLE IF NOT EXISTS session_task (idSession INTEGER, ' +
    'idTask INTEGER, taskNumber INTEGER, sent INTEGER DEFAULT 0,' +
    'PRIMARY KEY(idSession, idTask, taskNumber), ' +
    'FOREIGN KEY(idSession) REFERENCES session(idSession));');

  //Table for task_1 results
  db.execute('CREATE TABLE IF NOT EXISTS task_1 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, TpreTarget REAL NOT NULL, ' +
    'TpreTargetVariability REAL NOT NULL, Ttarget REAL NOT NULL, ' +
    'Treact REAL NOT NULL, AccTrial REAL NOT NULL, ' +
    'ReactTimeTrial REAL NOT NULL, sent INTEGER DEFAULT 0);');

  //Table for task_2 results
  db.execute('CREATE TABLE IF NOT EXISTS task_2 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, ' +
    'N INTEGER NOT NULL, K INTEGER NOT NULL, TpreTarget REAL NOT NULL, ' +
    'TpreTargetVariability REAL NOT NULL, Ttarget REAL NOT NULL, ' +
    'Treact REAL NOT NULL, AccTrial REAL NOT NULL, ' +
    'ReactTimeTrial REAL NOT NULL, sent INTEGER DEFAULT 0);');

  //Table for task_3 results
  db.execute('CREATE TABLE IF NOT EXISTS task_3 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, TpreTarget REAL NOT NULL, ' +
    'TpreTargetVariability REAL NOT NULL, Talert REAL NOT NULL, ' +
    'Ttarget REAL NOT NULL, Treact REAL NOT NULL, ' +
    'AccSiAlertTrial REAL NOT NULL, ReactTimeSiAlertTrial REAL NOT NULL,' +
    'AccNoAlertTrial REAL NOT NULL, ReactTimeNoAlertTrial REAL NOT NULL,' +
    'sent INTEGER DEFAULT 0);');

  //Table for task_4 results
  db.execute('CREATE TABLE IF NOT EXISTS task_4 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, Ptarget REAL NOT NULL, Tcue REAL NOT NULL, ' +
    'Ttarget REAL NOT NULL, Treact REAL NOT NULL, AccTrial REAL NOT NULL,' +
    'ReactTimeTrial REAL NOT NULL, AccCueTrial REAL NOT NULL, ' +
    'ReactTimeCueTrial REAL NOT NULL, AccStayTrial REAL NOT NULL, ' +
    'ReactTimeStayTrial REAL NOT NULL, AccChangeTrial REAL NOT NULL, ' +
    'ReactTimeChangeTrial REAL NOT NULL, sent INTEGER DEFAULT 0);');

  //Table for task_5 results
  db.execute('CREATE TABLE IF NOT EXISTS task_5 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, Ptarget REAL NOT NULL, Tcue REAL NOT NULL, ' +
    'Ttarget REAL NOT NULL, Treact REAL NOT NULL, ' +
    'AccValidTrial REAL NOT NULL, ReactTimeValidTrial REAL NOT NULL, ' +
    'AccInvalidTrial REAL NOT NULL, ReactTimeInvalidTrial REAL NOT NULL, ' +
    'sent INTEGER DEFAULT 0);');

  //Table for task_6 results
  db.execute('CREATE TABLE IF NOT EXISTS task_6 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, Tcue  REAL NOT NULL, TpreTargetMax REAL NOT NULL, ' +
    'TpreTargetMaxVariability REAL NOT NULL, TpreTargetMin REAL NOT NULL, ' +
    'TpreTargetMinVariability REAL NOT NULL, Ttarget REAL NOT NULL, ' +
    'Treact REAL NOT NULL, AccCueTmaxTrial REAL NOT NULL, ' +
    'ReactTimeCueTmaxTrial REAL NOT NULL, AccCueTminTrial REAL NOT NULL, ' +
    'ReactTimeCueTminTrial REAL NOT NULL, sent INTEGER DEFAULT 0);');

  //Table for task_7 results
  db.execute('CREATE TABLE IF NOT EXISTS task_7 (' +
    'idTask INTEGER PRIMARY KEY AUTOINCREMENT, N INTEGER NOT NULL, ' +
    'K INTEGER NOT NULL, TpreTarget REAL NOT NULL, ' +
    'TpreTargetVariability REAL NOT NULL, Ttarget REAL NOT NULL, ' +
    'Treact REAL NOT NULL, AccNOGOTrial REAL NOT NULL, ' +
    'AccGOTrial REAL NOT NULL, ReactTimeGOTrial REAL NOT NULL, ' +
    ' sent INTEGER DEFAULT 0);');

  /*Table db_version to manage database schema updates.*/
  db.execute('CREATE TABLE IF NOT EXISTS db_version (version INTEGER PRIMARY' +
    ' KEY, name TEXT);');

  /*Insert version = 0 in db_version if it is empty.*/
  db.execute('INSERT INTO db_version(version, name) SELECT 0, \'DB v. 1.0.0\'' +
  ' WHERE (SELECT count(*) FROM db_version) = 0;');

  checkDbVersion(db);

  db.close();
};

function checkDbVersion(db) {

  var sql = 'SELECT * from db_version;';
  Ti.API.debug('Performing: ' + sql);

  var res = db.execute(sql);

  if (res.isValidRow()) {
    var version = res.fieldByName('version');
    Ti.API.debug('DB version: ' + version + ' Current version: ' +
      Alloy.Globals.currentDBVersion);
    while (version < Alloy.Globals.currentDBVersion) {
      Ti.API.debug('Upgrading DB schema from version: ' + version + ' to: ' +
        ++version);
      upgradeDBSchema(db, version);
    }
  }

  res.close();
}

function upgradeDBSchema(db, version) {
  switch (version) {
    case 1:

      // Adding sent column to all tables
      var tables = [
        'session_task',
        'task_1',
        'task_2',
        'task_3',
        'task_4',
        'task_5',
        'task_6',
        'task_7',
      ];

      for (var i = 0; i < tables.length; i++) {
        if (!sentColumnExists(db, tables[i])) {
          Ti.API.debug('Adding sent to: ' + tables[i] + '...');
          db.execute('ALTER TABLE ' + tables[i] + ' ADD sent INTEGER DEFAULT 0;');
        } else {
          Ti.API.debug('sent column already exists in: ' + tables[i] + '...');
        }
      }

      db.execute('UPDATE db_version set version = 1, name = \'DB v. 1.0.1\';');
      break;
    default:
      Ti.API.warn('Wrong update version found, no update applied.');

  }
}

function sentColumnExists(db, table) {
  var result = false;
  var sql = 'PRAGMA table_info(' + table + ');';
  Ti.API.debug('Performing: ' + sql);
  var res = db.execute(sql);
  while (res.isValidRow()) {
    if (res.field(1) === 'sent') {
      result = true;
      break;
    }

    res.next();
  }

  res.close();
  return result;
}

// region: Patient
exports.createNewPatient = function (patientData) {
  Ti.API.debug('Creating a new patient: ' + JSON.stringify(patientData));

  var db = Ti.Database.open('SmartME');
  patientData.id = Ti.Platform.createUUID();

  var sql = 'INSERT INTO patient (idPatient, name, surname, gender, birthdate, '
    + 'educationYears, diagnosis, idDoctor) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';

  Ti.API.debug('db.execute(' + sql + ', ' + patientData.id + ', '
    + patientData.name + ', ' + patientData.surname + ', '
    + patientData.gender + ', ' + patientData.birthdate + ', '
    + patientData.educationYears + ', ' + patientData.diagnosis + ', '
    + patientData.idDoctor);

  db.execute(sql, patientData.id, patientData.name, patientData.surname,
    patientData.gender, patientData.birthdate, patientData.educationYears,
    patientData.diagnosis, patientData.idDoctor);

  sql = 'SELECT * FROM patient WHERE idPatient = ?;';

  var id = '';
  var res = db.execute(sql, patientData.id);

  if (res.isValidRow()) {
    id = res.fieldByName('idPatient');
  }

  res.close();
  db.close();

  return id;
};

exports.getPatientDays = function (idPatient) {
  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT DISTINCT dayDate FROM session where idPatient = ? GROUP BY dayDate';
  Ti.API.debug('Performing: ' + sql + ' idPatient: ' + idPatient);
  var res = db.execute(sql, idPatient);
  var records = [];

  while (res.isValidRow()) {
    records.push(res.field(0));
    res.next();
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(records));

  return records;
};

exports.getPatientList = function (idDoctor) {
  var db = Ti.Database.open('SmartME');
  var sql = '';
  var res;

  if (idDoctor) {
    sql = 'SELECT * FROM patient where idDoctor = ? ORDER BY surname ASC';
    Ti.API.debug('Performing: ' + sql + ' idDoctor: ' + idDoctor);
    res = db.execute(sql, idDoctor);
  } else {
    sql = 'SELECT * FROM patient ORDER BY surname ASC';
    Ti.API.debug('Performing: ' + sql);
    res = db.execute(sql);
  };

  var records = [];

  while (res.isValidRow()) {
    var patient = {};

    patient.idPatient = res.fieldByName('idPatient');
    patient.name = res.fieldByName('name');
    patient.surname = res.fieldByName('surname');
    patient.birthdate = res.fieldByName('birthdate');
    patient.gender = res.fieldByName('gender');
    patient.educationYears = res.fieldByName('educationYears');
    patient.diagnosis = res.fieldByName('diagnosis');
    patient.idDoctor = res.fieldByName('idDoctor');

    records.push(patient);
    res.next();
  }

  Ti.API.debug(JSON.stringify(records));
  res.close();
  db.close();
  return records;
};

// region: Session
exports.deleteSession = function (idSession) {
  /*
    1. Check session_task table where idSession;
    2. IF any record exists...
      2a. THEN: loop on previous resultSet to delete related task results.
        Delete current record from session_task;
    3. Delete the entry in the session table.
  */

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * from session_task WHERE idSession = ?;';
  Ti.API.debug('Performing: ' + sql + ' idSession: ' + idSession);

  var res = db.execute(sql, idSession);

  while (res.isValidRow()) {
    var idTask = res.fieldByName('idTask');
    var taskNumber = res.fieldByName('taskNumber');

    sql = 'DELETE FROM task_' + taskNumber + ' WHERE idTask = ?';
    Ti.API.debug('Performing: ' + sql + ' taskNumber: ' + taskNumber
      + ', idTask: ' + idTask);

    // Delete of record in task_x table.
    db.execute(sql, idTask);

    sql = 'DELETE FROM session_task WHERE idSession = ? AND idTask = ? '
      + ' AND taskNumber = ?;';
    Ti.API.debug('Performing: ' + sql + ' idSession: ' + idSession
      + ', idTask: ' + idTask + ', taskNumber: ' + taskNumber);

    // Delete of record in session_task table.
    db.execute(sql, idSession, idTask, taskNumber);
    res.next();
  }

  res.close();

  sql = 'DELETE FROM session WHERE idSession = ?;';
  Ti.API.debug('Performing: ' + sql + ' idSession: ' + idSession);

  // Delete session
  db.execute(sql, idSession);
  db.close();
};

exports.getSessionsResults = function (idPatient, dayDate, taskNumber,
  idSession) {

  var results = [];

  var db = Ti.Database.open('SmartME');
  var sql = '';
  var res;

  if (idSession) {
    sql = 'SELECT idSession FROM session WHERE idPatient = ? ' +
      'AND dayDate = ? AND idSession = ?;';

    Ti.API.debug('Performing: ' + sql + '[idPatient: ' + idPatient +
      ', dayDate: ' + dayDate + ', idSession: ' + idSession + ']');
    res = db.execute(sql, idPatient, dayDate, idSession);
  } else {
    sql = 'SELECT idSession FROM session WHERE idPatient = ? ' +
      'AND dayDate = ?;';

    Ti.API.debug('Performing: ' + sql + '[idPatient: ' + idPatient +
      ', dayDate: ' + dayDate + ']');

    res = db.execute(sql, idPatient, dayDate);
  }

  while (res.isValidRow()) {
    var record = {};
    record.idSession = res.fieldByName('idSession');
    record.resultsTrial = require('db').getTrialsResultsBySession(record.idSession,
      taskNumber);
    record.resultSession = computeResultSession(record.resultsTrial, taskNumber);

    results.push(record);
    res.next();
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(results));
  return results;
};

function computeResultSession(resultsTrial, taskNumber) {

  var result = {};
  var sumResults = {};

  resultsTrial.forEach(function (resultTrial) {
    for (var key in resultTrial) {
      if (key !== 'idTask') {
        if (!sumResults[key])
          sumResults[key] = 0;
        sumResults[key] += resultTrial[key];
      }
    }
  });

  for (var key in sumResults) {
    if (key !== 'idTask')
      result[key] = parseFloat(sumResults[key] / resultsTrial.length).toFixed(2);
  }

  return result;
}

exports.getTrialsResultsBySession = function (idSession, taskNumber) {
  var results = [];

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT idTask FROM session_task WHERE idSession = ? AND ' +
    'taskNumber = ?;';

  Ti.API.debug('Performing: ' + sql + '[idSession: ' + idSession +
    ', taskNumber: ' + taskNumber + ']');

  var res = db.execute(sql, idSession, taskNumber);
  var idTasks = [];

  while (res.isValidRow()) {
    idTasks.push(res.field(0));
    res.next();
  }

  res.close();
  db.close();

  switch (taskNumber) {
    case 1:
    case 2:
      results = getTaskResults(idTasks, taskNumber);
      break;
    case 3:
      results = getTask3Results(idTasks);
      break;
    case 4:
      results = getTask4Results(idTasks);
      break;
    case 5:
      results = getTask5Results(idTasks);
      break;
    case 6:
      results = getTask6Results(idTasks);
      break;
    case 7:
      results = getTask7Results(idTasks);
      break;
    default:
      Ti.API.warn('Unrecognized task number: ' + taskNumber + ' !!!');
  }

  return results;
};

exports.sessionEnd = function (idSession) {
  var endTs = new Date().toISOString();

  var db = Ti.Database.open('SmartME');
  var sql = 'UPDATE session SET endTs = ? WHERE idSession = ?;';

  Ti.API.debug('Performing: ' + sql + ' endTs: ' + endTs + ' idSession: '
    + idSession);
  db.execute(sql, endTs, idSession);
  db.close();
};

exports.sessionStart = function (idPatient, idDoctor) {
  var newSessionId = -1;
  var startTs = new Date().toISOString();
  var dayDate = startTs.substring(0, startTs.indexOf('T')).replace(/-/g, '');

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO session(idPatient, idDoctor, dayDate, startTs, sent) '
    + 'VALUES (?, ?, ?, ?, 0);';

  Ti.API.debug('Performing: ' + sql + 'idPatient: ' + idPatient
    + ' idDoctor: ' + idDoctor + ' dayDate: ' + dayDate
    + ' startTs: ' + startTs);

  db.execute(sql, idPatient, idDoctor, dayDate, startTs);
  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    newSessionId = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return newSessionId;
};

exports.getSessionsStatus = function (idDoctor) {
  var result = [];

  var db = Ti.Database.open('SmartME');
  var sql = '';
  var res;

  if (idDoctor) {
    sql = 'SELECT session.idSession, session.idDoctor, session.dayDate, session.endTs, ' +
      'patient.name AS patientName, patient.surname AS patientSurname, ' +
      'session.sent FROM session JOIN patient ON ' +
      'session.idPatient = patient.idPatient  WHERE session.idDoctor = ? AND session.endTs != ""';

    Ti.API.debug('Performing: ' + sql + ' idDoctor: ' + idDoctor);
    res = db.execute(sql, idDoctor);
  } else {
    sql = 'SELECT session.idSession, session.idDoctor, session.dayDate, session.endTs, ' +
      'patient.name AS patientName, patient.surname AS patientSurname, ' +
      'session.sent FROM session JOIN patient ON ' +
      'session.idPatient = patient.idPatient WHERE session.endTs != "";';

    Ti.API.debug('Performing: ' + sql);
    res = db.execute(sql);
  }

  while (res.isValidRow()) {
    var sessionStatus = {};
    sessionStatus.idSession = res.fieldByName('idSession');
    sessionStatus.idDoctor = res.fieldByName('idDoctor');
    sessionStatus.dayDate = res.fieldByName('dayDate');
    sessionStatus.patientName = res.fieldByName('patientName');
    sessionStatus.patientSurname = res.fieldByName('patientSurname');
    sessionStatus.sent = res.fieldByName('sent');

    result.push(sessionStatus);
    res.next();
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));
  return result;
};

exports.getSessionTasksToSend = function (idSession) {
  var results = [];

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM session_task WHERE idSession = ? and sent = 0;';

  Ti.API.debug('Performing: ' + sql + '[idSession: ' + idSession + ']');
  var res = db.execute(sql, idSession);

  while (res.isValidRow()) {
    var sessionTask = {};
    sessionTask.idSession = res.fieldByName('idSession');
    sessionTask.idTask = res.fieldByName('idTask');
    sessionTask.taskNumber = res.fieldByName('taskNumber');

    results.push(sessionTask);
    res.next();
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(results));
  return results;
};

// region: Task
exports.getDayTasks = function (idPatient, dayDate) {
  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT idSession FROM session where idPatient = ? AND dayDate = ?';

  Ti.API.debug('Performing: ' + sql + ' [idPatient: ' + idPatient +
    ', dayDate: ' + dayDate + ']');

  var res = db.execute(sql, idPatient, dayDate);
  var records = [];

  while (res.isValidRow()) {
    var idSession = res.field(0);
    sql = 'SELECT DISTINCT taskNumber FROM session_task where idSession = ? GROUP BY taskNumber;';

    Ti.API.debug('Performing: ' + sql + ' [idSession: ' + idSession + ']');

    var res1 = db.execute(sql, idSession);
    while (res1.isValidRow()) {
      records.push(res1.field(0));
      res1.next();
    }

    res1.close();
    res.next();
  }

  res.close();
  db.close();

  // Delete duplicate elements from records array
  records = records.filter(function (elem, index, self) {
    return index == self.indexOf(elem);
  });

  Ti.API.debug(JSON.stringify(records));

  return records;
};

exports.getTaskResult = function (idTask, taskNumber) {

  var result = {};

  switch (taskNumber) {
    case 1:
    case 2:
      result = getTask1Or2Result(idTask, taskNumber);
      break;
    case 3:
      result = getTask3Result(idTask);
      break;
    case 4:
      result = getTask4Result(idTask);
      break;
    case 5:
      result = getTask5Result(idTask);
      break;
    case 6:
      result = getTask6Result(idTask);
      break;
    case 7:
      result = getTask7Result(idTask);
      break;
    default:
      Ti.API.warn('Unrecognized task number: ' + taskNumber + ' !!!');
  }

  return result;
};

exports.saveTask = function (idSession, taskNumber, taskData) {
  var result = -1;
  var idTask = -1;

  switch (taskNumber) {
    case 1:
      idTask = saveTask1(taskData);
      break;
    case 2:
      idTask = saveTask2(taskData);
      break;
    case 3:
      idTask = saveTask3(taskData);
      break;
    case 4:
      idTask = saveTask4(taskData);
      break;
    case 5:
      idTask = saveTask5(taskData);
      break;
    case 6:
      idTask = saveTask6(taskData);
      break;
    case 7:
      idTask = saveTask7(taskData);
      break;
    default:
      Ti.API.warn('Unrecognized task number: ' + taskNumber + ' !!!');
  }

  if (idTask !== -1) {
    var db = Ti.Database.open('SmartME');
    var sql = 'INSERT INTO session_task(idSession, idTask, taskNumber) ' +
      'VALUES (?, ?, ?);';

    Ti.API.debug('Performing: ' + sql + '[idSession: ' + idSession +
      ', idTask: ' + idTask + ', taskNumber: ' + taskNumber + ']');

    db.execute(sql, idSession, idTask, taskNumber);
    db.close();
  }

  return idTask;
};

function getTask1Or2Result(idTask, taskNumber) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_' + taskNumber + ' where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.TpreTarget = res.fieldByName('TpreTarget');
    result.TpreTargetVariability =
      res.fieldByName('TpreTargetVariability');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccTrial = res.fieldByName('AccTrial');
    result.ReactTimeTrial = res.fieldByName('ReactTimeTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTask3Result(idTask) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_3 where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.TpreTarget = res.fieldByName('TpreTarget');
    result.TpreTargetVariability =
      res.fieldByName('TpreTargetVariability');
    result.Talert = res.fieldByName('Talert');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccSiAlertTrial =
      res.fieldByName('AccSiAlertTrial');
    result.ReactTimeSiAlertTrial =
      res.fieldByName('ReactTimeSiAlertTrial');
    result.AccNoAlertTrial =
      res.fieldByName('AccNoAlertTrial');
    result.ReactTimeNoAlertTrial =
      res.fieldByName('ReactTimeNoAlertTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTask4Result(idTask) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_4 where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.Ptarget = res.fieldByName('Ptarget');
    result.Tcue = res.fieldByName('Tcue');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccTrial = res.fieldByName('AccTrial');
    result.ReactTimeTrial = res.fieldByName('ReactTimeTrial');
    result.AccCueTrial = res.fieldByName('AccCueTrial');
    result.ReactTimeCueTrial =
      res.fieldByName('ReactTimeCueTrial');
    result.AccStayTrial = res.fieldByName('AccStayTrial');
    result.ReactTimeStayTrial =
      res.fieldByName('ReactTimeStayTrial');
    result.AccChangeTrial = res.fieldByName('AccChangeTrial');
    result.ReactTimeChangeTrial =
      res.fieldByName('ReactTimeChangeTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTask5Result(idTask) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_5 where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.Ptarget = res.fieldByName('Ptarget');
    result.Tcue = res.fieldByName('Tcue');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccValidTrial = res.fieldByName('AccValidTrial');
    result.ReactTimeValidTrial =
      res.fieldByName('ReactTimeValidTrial');
    result.AccInvalidTrial =
      res.fieldByName('AccInvalidTrial');
    result.ReactTimeInvalidTrial =
      res.fieldByName('ReactTimeInvalidTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTask6Result(idTask) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_6 where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.Tcue = res.fieldByName('Tcue');
    result.TpreTargetMax = res.fieldByName('TpreTargetMax');
    result.TpreTargetMaxVariability =
      res.fieldByName('TpreTargetMaxVariability');
    result.TpreTargetMin = res.fieldByName('TpreTargetMin');
    result.TpreTargetMinVariability =
      res.fieldByName('TpreTargetMinVariability');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccCueTmaxTrial =
      res.fieldByName('AccCueTmaxTrial');
    result.ReactTimeCueTmaxTrial =
      res.fieldByName('ReactTimeCueTmaxTrial');
    result.AccCueTminTrial =
      res.fieldByName('AccCueTminTrial');
    result.ReactTimeCueTminTrial =
      res.fieldByName('ReactTimeCueTminTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTask7Result(idTask) {
  var results = {};

  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM task_7 where idtask = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');
  var res = db.execute(sql, idTask);
  var result = {};

  if (res.isValidRow()) {
    result.idTask = res.fieldByName('idTask');
    result.N = res.fieldByName('N');
    result.K = res.fieldByName('K');
    result.TpreTarget = res.fieldByName('TpreTarget');
    result.TpreTargetVariability =
      res.fieldByName('TpreTargetVariability');
    result.Ttarget = res.fieldByName('Ttarget');
    result.Treact = res.fieldByName('Treact');
    result.AccNOGOTrial = res.fieldByName('AccNOGOTrial');
    result.AccGOTrial = res.fieldByName('AccGOTrial');
    result.ReactTimeGOTrial =
      res.fieldByName('ReactTimeGOTrial');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
}

function getTaskResults(idTasks, taskNumber) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_' + taskNumber + ' WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.TpreTarget = res.fieldByName('TpreTarget');
      trialsResultsBySession.TpreTargetVariability =
        res.fieldByName('TpreTargetVariability');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccTrial = res.fieldByName('AccTrial');
      trialsResultsBySession.ReactTimeTrial = res.fieldByName('ReactTimeTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function getTask3Results(idTasks) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_3 WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.TpreTarget = res.fieldByName('TpreTarget');
      trialsResultsBySession.TpreTargetVariability =
        res.fieldByName('TpreTargetVariability');
      trialsResultsBySession.Talert = res.fieldByName('Talert');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccSiAlertTrial =
        res.fieldByName('AccSiAlertTrial');
      trialsResultsBySession.ReactTimeSiAlertTrial =
        res.fieldByName('ReactTimeSiAlertTrial');
      trialsResultsBySession.AccNoAlertTrial =
        res.fieldByName('AccNoAlertTrial');
      trialsResultsBySession.ReactTimeNoAlertTrial =
        res.fieldByName('ReactTimeNoAlertTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function getTask4Results(idTasks) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_4 WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.Ptarget = res.fieldByName('Ptarget');
      trialsResultsBySession.Tcue = res.fieldByName('Tcue');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccTrial = res.fieldByName('AccTrial');
      trialsResultsBySession.ReactTimeTrial = res.fieldByName('ReactTimeTrial');
      trialsResultsBySession.AccCueTrial = res.fieldByName('AccCueTrial');
      trialsResultsBySession.ReactTimeCueTrial =
        res.fieldByName('ReactTimeCueTrial');
      trialsResultsBySession.AccStayTrial = res.fieldByName('AccStayTrial');
      trialsResultsBySession.ReactTimeStayTrial =
        res.fieldByName('ReactTimeStayTrial');
      trialsResultsBySession.AccChangeTrial = res.fieldByName('AccChangeTrial');
      trialsResultsBySession.ReactTimeChangeTrial =
        res.fieldByName('ReactTimeChangeTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function getTask5Results(idTasks) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_5 WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.Ptarget = res.fieldByName('Ptarget');
      trialsResultsBySession.Tcue = res.fieldByName('Tcue');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccValidTrial = res.fieldByName('AccValidTrial');
      trialsResultsBySession.ReactTimeValidTrial =
        res.fieldByName('ReactTimeValidTrial');
      trialsResultsBySession.AccInvalidTrial =
        res.fieldByName('AccInvalidTrial');
      trialsResultsBySession.ReactTimeInvalidTrial =
        res.fieldByName('ReactTimeInvalidTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function getTask6Results(idTasks) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_6 WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.Tcue = res.fieldByName('Tcue');
      trialsResultsBySession.TpreTargetMax = res.fieldByName('TpreTargetMax');
      trialsResultsBySession.TpreTargetMaxVariability =
        res.fieldByName('TpreTargetMaxVariability');
      trialsResultsBySession.TpreTargetMin = res.fieldByName('TpreTargetMin');
      trialsResultsBySession.TpreTargetMinVariability =
        res.fieldByName('TpreTargetMinVariability');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccCueTmaxTrial =
        res.fieldByName('AccCueTmaxTrial');
      trialsResultsBySession.ReactTimeCueTmaxTrial =
        res.fieldByName('ReactTimeCueTmaxTrial');
      trialsResultsBySession.AccCueTminTrial =
        res.fieldByName('AccCueTminTrial');
      trialsResultsBySession.ReactTimeCueTminTrial =
        res.fieldByName('ReactTimeCueTminTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function getTask7Results(idTasks) {
  var results = [];
  idTasks.forEach(function (idTask) {
    var db = Ti.Database.open('SmartME');
    var sql = 'SELECT * FROM task_7 WHERE idTask = ?; ';

    Ti.API.debug('Performing: ' + sql + '[idTask: ' + idTask + ']');

    var res = db.execute(sql, idTask);
    var idTasks = [];

    while (res.isValidRow()) {
      var trialsResultsBySession = {};
      trialsResultsBySession.idTask = res.fieldByName('idTask');
      trialsResultsBySession.N = res.fieldByName('N');
      trialsResultsBySession.K = res.fieldByName('K');
      trialsResultsBySession.TpreTarget = res.fieldByName('TpreTarget');
      trialsResultsBySession.TpreTargetVariability =
        res.fieldByName('TpreTargetVariability');
      trialsResultsBySession.Ttarget = res.fieldByName('Ttarget');
      trialsResultsBySession.Treact = res.fieldByName('Treact');
      trialsResultsBySession.AccNOGOTrial = res.fieldByName('AccNOGOTrial');
      trialsResultsBySession.AccGOTrial = res.fieldByName('AccGOTrial');
      trialsResultsBySession.ReactTimeGOTrial =
        res.fieldByName('ReactTimeGOTrial');

      results.push(trialsResultsBySession);
      res.next();
    }

    res.close();
    db.close();

  });

  return results;
}

function saveTask1(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_1(N, K, TpreTarget, TpreTargetVariability, ' +
    'Ttarget, Treact, AccTrial, ReactTimeTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.TpreTarget,
    taskData.TpreTargetVariability, taskData.Ttarget, taskData.Treact,
    taskData.AccTrial, taskData.ReactTimeTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask2(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_2(N, K, TpreTarget, TpreTargetVariability, ' +
    'Ttarget, Treact, AccTrial, ReactTimeTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.TpreTarget,
    taskData.TpreTargetVariability, taskData.Ttarget, taskData.Treact,
    taskData.AccTrial, taskData.ReactTimeTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask3(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_3(N, K, TpreTarget, TpreTargetVariability, ' +
    'Talert, Ttarget, Treact, AccSiAlertTrial, ReactTimeSiAlertTrial, ' +
    'AccNoAlertTrial, ReactTimeNoAlertTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.TpreTarget,
    taskData.TpreTargetVariability, taskData.Talert, taskData.Ttarget,
    taskData.Treact, taskData.AccSiAlertTrial, taskData.ReactTimeSiAlertTrial,
    taskData.AccNoAlertTrial, taskData.ReactTimeNoAlertTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask4(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_4(N, K, Ptarget, Tcue, Ttarget, Treact, ' +
    'AccTrial, ReactTimeTrial, AccCueTrial, ReactTimeCueTrial, AccStayTrial, ' +
    'ReactTimeStayTrial, AccChangeTrial, ReactTimeChangeTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.Ptarget, taskData.Tcue,
    taskData.Ttarget, taskData.Treact, taskData.AccTrial,
    taskData.ReactTimeTrial, taskData.AccCueTrial, taskData.ReactTimeCueTrial,
    taskData.AccStayTrial, taskData.ReactTimeStayTrial, taskData.AccChangeTrial,
    taskData.ReactTimeChangeTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask5(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_5(N, K, Ptarget, Tcue, Ttarget, Treact, ' +
   'AccValidTrial, ReactTimeValidTrial, AccInvalidTrial, ' +
   'ReactTimeInvalidTrial) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.Ptarget, taskData.Tcue,
    taskData.Ttarget, taskData.Treact, taskData.AccValidTrial,
    taskData.ReactTimeValidTrial, taskData.AccInvalidTrial,
    taskData.ReactTimeInvalidTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask6(taskData) {
  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_6(N, K, Tcue, TpreTargetMax, ' +
    'TpreTargetMaxVariability, TpreTargetMin, TpreTargetMinVariability, ' +
    'Ttarget, Treact, AccCueTmaxTrial, ReactTimeCueTmaxTrial, ' +
    'AccCueTminTrial, ReactTimeCueTminTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.Tcue, taskData.TpreTargetMax,
    taskData.TpreTargetMaxVariability, taskData.TpreTargetMin,
    taskData.TpreTargetMinVariability, taskData.Ttarget, taskData.Treact,
    taskData.AccCueTmaxTrial, taskData.ReactTimeCueTmaxTrial,
    taskData.AccCueTminTrial, taskData.ReactTimeCueTminTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

function saveTask7(taskData) {

  var result = -1;

  var db = Ti.Database.open('SmartME');
  var sql = 'INSERT INTO task_7(N, K, TpreTarget, TpreTargetVariability, ' +
    'Ttarget, Treact, AccNOGOTrial, AccGOTrial, ReactTimeGOTrial) ' +
    'VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);';

  Ti.API.debug('Performing: ' + sql + 'taskData: ' + JSON.stringify(taskData));

  db.execute(sql, taskData.N, taskData.K, taskData.TpreTarget,
    taskData.TpreTargetVariability, taskData.Ttarget, taskData.Treact,
    taskData.AccNOGOTrial, taskData.AccGOTrial, taskData.ReactTimeGOTrial);

  var res = db.execute('SELECT last_insert_rowid()');
  if (res.isValidRow()) {
    result = res.field(0);
    res.close();
    db.close();
  } else {
    db.close();
  }

  return result;
}

exports.getSessionTasks = function (idSession) {
  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT taskNumber FROM session_task where idSession = ?';
  Ti.API.debug('Performing: ' + sql + ' idSession: ' + idSession);
  var res = db.execute(sql, idSession);
  var records = [];

  while (res.isValidRow()) {
    records.push(res.field(0));
    res.next();
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(records));

  return records;
};

exports.getSessionData = function (idSession) {
  var db = Ti.Database.open('SmartME');
  var sql = 'SELECT * FROM session where idSession = ? AND sent = 0';
  Ti.API.debug('Performing: ' + sql + ' idSession: ' + idSession);
  var res = db.execute(sql, idSession);
  var result = {};

  if (res.isValidRow()) {
    result.idSession = res.fieldByName('idSession');
    result.idPatient = res.fieldByName('idPatient');
    result.idDoctor = res.fieldByName('idDoctor');
    result.startTs = res.fieldByName('startTs');
    result.endTs = res.fieldByName('endTs');
    result.dayDate = res.fieldByName('dayDate');
    result.sent = res.fieldByName('sent');
  }

  res.close();
  db.close();

  Ti.API.debug(JSON.stringify(result));

  return result;
};

exports.setSent = function (keys, table) {

  var db = Ti.Database.open('SmartME');
  var sql;

  if (table.indexOf('session_task') !== -1) {
    sql = 'UPDATE ' +  table + ' SET sent = 1 WHERE idSession = ? and idTask = ?;';
    Ti.API.debug('Performing: ' + sql + ' idSession: ' + keys[0] +
      ' idTask: ' + keys[1]);
    db.execute(sql, keys[0], keys[1]);
  } else if (table.indexOf('session') !== -1) {
    sql = 'UPDATE ' + table + ' SET sent = 1 WHERE idSession = ?;';
    Ti.API.debug('Performing: ' + sql + ' idSession: ' + keys[0]);
    db.execute(sql, keys[0]);
  } else {
    sql = 'UPDATE ' + table + ' SET sent = 1 WHERE idTask = ?;';
    Ti.API.debug('Performing: ' + sql + ' idtask: ' + keys[0]);
    db.execute(sql, keys[0]);
  }

  db.close();

};
