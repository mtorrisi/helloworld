function getSessionData(idSession) {
  var data = {};

  data.session = require('db').getSessionData(idSession);
  data.session_task = require('db').getSessionTasksToSend(idSession);

  Ti.API.debug(JSON.stringify(data));
  return data;
};

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}

function postData(data, collection, params, _callback) {

  var client = Ti.Network.createHTTPClient({
    onload: function (e) {
      Ti.API.debug('Received text: ' + this.responseText);
      _callback(JSON.parse(this.responseText), params);
    },

    // function called when an error occurs, including a timeout
    onerror: function (e) {
      Ti.API.error(e.error);
      if (params.length) { //is going to send task_x results or session_tasks...
        if (params.index === params.length - 1) {
          // then invoke _callback only for last item
          params._callback(false);
        }
      } else {
        params._callback(false);
      }
    },

    timeout: 25000,  // in milliseconds
  });

  // Prepare the connection.
  client.open('POST', gLibraryEndpoint + '/repos/smartme/' + collection);

  client.setRequestHeader('Authorization', exports.token);

  data.deviceId = Ti.App.Properties.getString('deviceId', '');

  // Send the request.
  client.send(data);

}

function resultCallback(e, params) {
  Ti.API.debug('Task_' +
  params.data.session_task[params.index].taskNumber + ' sent.');

  var keys = [e.idTask];
  require('db').setSent(keys, 'task_' +
  params.data.session_task[params.index].taskNumber);

  postData(params.data.session_task[params.index], 'session_task',
    params, sessionTaskCallBack);
}

function sessionCallback(e, params) {
  Ti.API.debug('Session sent: ' + JSON.stringify(e));

  var keys = [e.idSession];
  require('db').setSent(keys, 'session');
  params._callback(true);
}

function sessionTaskCallBack(e, params) {
  Ti.API.debug('Session Task sent: ' + JSON.stringify(e));

  var keys = [e.idSession, e.idTask];
  require('db').setSent(keys, 'session_task');

  if (params.index === params.length - 1) {
    var remaingData = require('db').getSessionTasksToSend(params.data.session.idSession);
    if (Object.keys(remaingData).length === 0) {
      postData(params.data.session, 'session', params, sessionCallback);
    } else {
      Ti.API.warn('Some data was not submitted. They will be posted on next ' +
        'submission.');
      params._callback(false);
    }
  }
}

exports.token = Ti.App.Properties.getString('token', '');
var gLibraryEndpoint = 'https://glibrary.ct.infn.it:3500/v2';

exports.checkToken = function (_callback) {
  Ti.API.debug('checkToken called...');

  var client = Ti.Network.createHTTPClient({
    onload: function (e) {
      var response = JSON.parse(this.responseText);
      _callback(response);
    },

    // function called when an error occurs, including a timeout
    onerror: function (e) {
      Ti.API.error(e.error);
      _callback(e);
    },

    timeout: 5000,  // in milliseconds
  });

  client.setRequestHeader('Authorization', exports.token);

  // Prepare the connection.
  client.open('GET', gLibraryEndpoint + '/repos/smartme');

  // Send the request.
  client.send();
};

exports.login = function (username, password, _callback) {
  Ti.API.debug('Login called...');

  var client = Ti.Network.createHTTPClient({
    onload: function (e) {
      var response = JSON.parse(this.responseText);
      Ti.API.debug('Token: ' + response.id);
      Ti.App.Properties.setString('token', response.id);
      exports.token = response.id;
      _callback(response);
    },

    // function called when an error occurs, including a timeout
    onerror: function (e) {
      Ti.API.error(e.error);
      _callback(e);
    },

    timeout: 25000,  // in milliseconds
  });

  // Prepare the connection.
  client.open('POST', gLibraryEndpoint + '/users/login');

  // Send the request.
  client.send({ username: username, password: password });

};

exports.sendSessionData = function (idSession, _callback) {
  var data = getSessionData(idSession);

  var taskNumbers = [];

  if (data.session_task.length) {
    for (var i = 0; i < data.session_task.length; i++) {
      if (!isInArray(data.session_task[i].taskNumber, taskNumbers)) {
        var result = require('db').getTaskResult(data.session_task[i].idTask,
          data.session_task[i].taskNumber);

        var params = { length:  data.session_task.length, index: i, data: data };
        params._callback = function (e) { _callback(e); };

        if (Object.keys(result).length !== 0) {//POST task result and session_task
          postData(result, 'task_' + data.session_task[i].taskNumber,
            params, resultCallback);
        } else { // POST just session_task
          postData(params.data.session_task[params.index], 'session_task',
            params, sessionTaskCallBack);
        }
      }
    }
  } else if (data.session !== undefined && Object.keys(data.session).length !== 0) {
    data._callback = function (e) { _callback(e); };

    // session_task and results war already sent, so post just session.
    postData(data.session, 'session', data, sessionCallback);
  } else {
    Ti.API.warn('Nothing to send!');
  }
};
