var args = $.args;
var selectedGender = $.genderPIK.setSelectedRow(0, 0, false);

function createPatient(e) {
  var tmp = $.birthdateTXT.value.split('/');
  var birthdate = new Date(tmp[2], tmp[1] - 1, tmp[0]);
  var patientData = {};
  patientData.name = $.nameTXT.value;
  patientData.surname = $.surnameTXT.value;
  patientData.birthdate = birthdate.toISOString();
  patientData.gender = selectedGender[0];
  patientData.diagnosis = $.diagnosisTXT.value;
  patientData.idDoctor = $.idDoctorTXT.value;
  var id = require('db').createNewPatient(patientData);

  Ti.API.info('Added Patient: ' + JSON.stringify(patientData));
  if (id !== '') {
    Ti.UI.createAlertDialog({
      message: 'New Patient successfully added.',
      ok: 'OK',
      title: 'Message',
    }).show();
    $.nameTXT.value = '';
    $.surnameTXT.value = '';
    $.birthdateTXT.value = '';
    $.genderPIK.setSelectedRow(0, 0, false);
    $.diagnosisTXT.value = '';
    $.educationYearsTXT.value = '';
  }
};

function listPatient(e) {
  var patients = require('db').getPatientList($.idDoctorTXT.value);

  patients.forEach(function (entry) {
    Ti.API.info(JSON.stringify(entry));
  });

  Ti.UI.createAlertDialog({
    message: '# Patients: ' + patients.length,
    ok: 'OK',
    title: 'Message',
  }).show();
}

function listPatientDay(e) {
  var dayDates = require('db').getPatientDays('50b7ebdd-1356-474c-99d6-574434c4341a');

  dayDates.forEach(function (entry) {
    Ti.API.info(JSON.stringify(entry));
  });

  Ti.UI.createAlertDialog({
    message: '# Day dates: ' + dayDates.length,
    ok: 'OK',
    title: 'Message',
  }).show();
}

function showDatePickerDialog(e) {
  var picker = Ti.UI.createPicker({
    type: Ti.UI.PICKER_TYPE_DATE,
    minDate: new Date(1900, 0, 1),
    maxDate: new Date(9999, 11, 31),
  });

  picker.showDatePickerDialog({
    value: new Date(),
    callback: function (e) {
      if (e.cancel) {
        Ti.API.info('User canceled dialog');
      } else {
        Ti.API.info('User selected date: ' + e.value);
        $.birthdateTXT.value = e.value.formatDDMMYYYY();
      }
    },
  });
}

function getSelectedGender(e) {
  Ti.API.info(e.selectedValue);
  selectedGender = e.selectedValue;
}

Date.prototype.formatDDMMYYYY = function () {
  return ('0' + this.getDate()).slice(-2) +
    '/' + ('0' + (this.getMonth() + 1)).slice(-2) +
    '/' + this.getFullYear();
};
