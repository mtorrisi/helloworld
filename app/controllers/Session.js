// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

function startSession(e) {
  var sessionId =
    require('db').sessionStart('50b7ebdd-1356-474c-99d6-574434c4341a',
    'mtorrisi');

  var msg = '';
  if (sessionId !== -1) {
    msg = 'New session successfully started, sessionId: ' + sessionId;
  } else {
    msg = 'Fail to start session';
  }

  Ti.UI.createAlertDialog({
    message: msg,
    ok: 'OK',
    title: 'Message',
  }).show();

}

function endSession(e) {
  require('db').sessionEnd(2);
  Ti.UI.createAlertDialog({
    message: 'Session completed.',
    ok: 'OK',
    title: 'Message',
  }).show();
}

function deleteSession(e) {
  require('db').deleteSession(2);
  Ti.UI.createAlertDialog({
    message: 'Session deleted.',
    ok: 'OK',
    title: 'Message',
  }).show();
}

function sessionStatus(e) {
  var doctorId;
  var btnId = e.source.id;
  var records = [];

  if (btnId === 'sessionStatusBTN') {
    records = require('db').sessionStatus();
  } else {
    doctorId = 'mtorrisi';
    records = require('db').sessionStatus(doctorId);
  }

  Ti.API.info(JSON.stringify(records));
  Ti.UI.createAlertDialog({
    message: (btnId !== 'sessionStatusBTN') ?
      records.length + ' sessions for doctor: ' + doctorId :
      records.length + ' sessions for all doctors.',
    ok: 'OK',
    title: 'Message',
  }).show();
}

function getTrialsResultsBySession(e) {
  var records = require('db').getTrialsResultsBySession(1, 7);

  records.forEach(function (entry) {
    Ti.API.info(JSON.stringify(entry));
  });
}

function getSessionsResults(e) {
  var records = require('db').getSessionsResults(
    '115edefd-8c3d-4ea4-a764-7a8894579c45', '20160806', 1, 1);

  records.forEach(function (entry) {
    Ti.API.info(JSON.stringify(entry));
  });
}
