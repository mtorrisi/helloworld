// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;

var tasks = [
  {
    N: 10,
    K: 5,
    TpreTarget: 5.236,
    TpreTargetVariability: 2,
    Ttarget: 0.221,
    Treact: 0.12,
    AccTrial: 0.5,
    ReactTimeTrial: 1.02,
  },
  {
    N: 10,
    K: 8,
    TpreTarget: 5.4,
    TpreTargetVariability: 3,
    Ttarget: 0.2,
    Treact: 0.15,
    AccTrial: 0.8,
    ReactTimeTrial: 1.885,
  },
  {
    N: 10,
    K: 8,
    TpreTarget: 5.13,
    TpreTargetVariability: 3,
    Talert: 0.2,
    Ttarget: 0.2,
    Treact: 0.15,
    AccSiAlertTrial: 1.26,
    ReactTimeSiAlertTrial: 1.5698,
    AccNoAlertTrial: 1.326,
    ReactTimeNoAlertTrial: 5.124,
  },
  {
    N: 10,
    K: 8,
    Ptarget: 0.23,
    Tcue: 0.68,
    Ttarget: 0.2,
    Treact: 0.15,
    AccTrial: 2.23,
    ReactTimeTrial: 5.123,
    AccCueTrial: 5.21,
    ReactTimeCueTrial: 3.12,
    AccStayTrial: 4.194,
    ReactTimeStayTrial: 1.742,
    AccChangeTrial: 2.362,
    ReactTimeChangeTrial: 7.312,
  },
  {
    N: 10,
    K: 8,
    Ptarget: 0.23,
    Tcue: 0.68,
    Ttarget: 0.2,
    Treact: 0.15,
    AccValidTrial: 8.125,
    ReactTimeValidTrial: 7.132,
    AccInvalidTrial: 7.123,
    ReactTimeInvalidTrial: 4.823,
  },
  {
    N: 10,
    K: 8,
    Tcue: 0.68,
    TpreTargetMax: 2.365,
    TpreTargetMaxVariability: 1.265,
    TpreTargetMin: 4.1203,
    TpreTargetMinVariability: 2.102,
    Ttarget: 1.4021,
    Treact: 0.123,
    AccCueTmaxTrial: 1.125,
    ReactTimeCueTmaxTrial: 2.25,
    AccCueTminTrial: 4.75,
    ReactTimeCueTminTrial: 3.201,
  },
  {
    N: 10,
    K: 8,
    TpreTarget: 12,
    TpreTargetVariability: 0.23,
    Ttarget: 1.201,
    Treact: 0.412,
    AccNOGOTrial: 9.214,
    AccGOTrial: 1.1235,
    ReactTimeGOTrial: 2.102,
  },
];

function callSaveTask(e) {
  var btnId = e.source.id;
  var taskNumber = parseInt(btnId.split('_')[1]);
  require('db').saveTask(1, taskNumber, tasks[taskNumber - 1]);
}

function callGetDayTasks(e) {
  require('db').getDayTasks('115edefd-8c3d-4ea4-a764-7a8894579c45', '20160806');
}
